/*
 * This file was generated by dbusxml2cpp version 0.6
 * Command line was: dbusxml2cpp -v -m -i kglobalshortcutinfo_p.h -p kdedglobalaccel_component_interface org.kde.kdedglobalaccel.Component.xml org.kde.kdedglobalaccel.Component
 *
 * dbusxml2cpp is Copyright (C) 2008 Nokia Corporation and/or its subsidiary(-ies).
 *
 * This is an auto-generated file.
 * This file may have been hand-edited. Look for HAND-EDIT comments
 * before re-generating it.
 */

#include "kdedglobalaccel_component_interface.h"

/*
 * Implementation of interface class OrgKdeKdedglobalaccelComponentInterface
 */

OrgKdeKdedglobalaccelComponentInterface::OrgKdeKdedglobalaccelComponentInterface(const QString &service, const QString &path, const QDBusConnection &connection, QObject *parent)
    : QDBusAbstractInterface(service, path, staticInterfaceName(), connection, parent)
{
}

OrgKdeKdedglobalaccelComponentInterface::~OrgKdeKdedglobalaccelComponentInterface()
{
}


#include "kdedglobalaccel_component_interface.moc"
